/*************************************************************************************************************
 * Filename: otp_enc_d.c
 * Author: Jens Bodal
 * Date: June 5, 2016
 * Description: some code reused from my CS 372 assignment
 ************************************************************************************************************/

#include <arpa/inet.h>  // inet_ntop
#include <stdlib.h>     // atoi, EXIT_FAILURE, EXIT_SUCCESS, NULL
#include <dirent.h>     // DIR, dirent, opendir, readdir
#include <limits.h>     // PATH_MAX
#include <netdb.h>      // addrinfo, getaddrinfo
#include <signal.h>     // signal, SIGINT
#include <stdio.h>      // printf, fprintf
#include <stdlib.h>     // exit
#include <string.h>     // memset
#include <sys/errno.h>  // perror, errno
#include <sys/socket.h> // socket
#include <sys/types.h>  // not required
#include <sys/signal.h> // sigaction
#include <sys/wait.h>   // waitpid, WNOHANG
#include <unistd.h>     // close, read, write (macros defined by integrated file system), access

#include "otp_d.h"
#include "otp_engine.h"

static const int START_PORT = 1;
static const int END_PORT = 65535;
static const int BACKLOG = 10;

int main(int argc, char *argv[]) {
    char* port;
    
    /* Validate command line arguments and look for port then start server */
    if (argc != 2) {
        fprintf(stderr, "Please enter the port for the server to run on\n");
        fprintf(stderr, "You must also specify '&' so that this runs in the background\n");
        return (1);
    }
    else {
       port = argv[1]; 
    }

    if (!isValidPort(port)) {
        fprintf(stderr, "Please enter a port between %d-%d\n", START_PORT, END_PORT);
    }
    else {
        /* main entry point */
        startServer(port);
    }

    return EXIT_SUCCESS;
}

int isValidPort(char *port) {
    int p = atoi(port);
    if (p < START_PORT || p > END_PORT) {
        return 0;
    }
    return 1;
}

void startServer(char *port) {
    int serverSocket;               // local listening socket file descriptor
    struct addrinfo *addressList;   // linkedlist of possible addrinfo structs to bind to
    struct sigaction sa;

    if ((setAddressList(NULL, port, &addressList) == 1) && (bindOrConnect(&serverSocket, &addressList, 1) != 1)) {
        exit(EXIT_FAILURE);
    }
    
    /* Attempt to listen on socket */
    if (listen(serverSocket, BACKLOG) == -1) {
        perror("Error listening on socket");
        exit(EXIT_FAILURE);
    }
    
    /* reap all dead processes */
    sa.sa_handler = sigchild_handler;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = SA_RESTART;
    if (sigaction(SIGCHLD, &sa, NULL) == -1) {
        perror("sigaction");
        exit(EXIT_FAILURE);
    }

    waitForConnection(serverSocket, &port);
}

int setAddressList(char *host, char *port, struct addrinfo **addressList) {
    struct addrinfo socketAddress;              // specifications for type of socket address
    
    /* Fill in local socket address structure */
    memset(&socketAddress, 0, sizeof socketAddress);    
    socketAddress.ai_family = AF_UNSPEC;        // Use any address specification
    socketAddress.ai_socktype = SOCK_STREAM;    // Two-way connection-based byte stream
    
    /* If host is NULL we will automatically obtain an IP */
    if (host == NULL) {
        socketAddress.ai_flags = AI_PASSIVE;    // Automatically obtain IP
    }
    
    /* fill in addressList with possible addresses */
    int addrInfoError;              
    if ((addrInfoError = getaddrinfo(host, port, &socketAddress, addressList)) != 0) {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(addrInfoError));
        return 0;
    }
    return 1;
}

int bindOrConnect(int *socketfd, struct addrinfo **addressList, int doBind) {
    struct addrinfo *addr;          // current node in addressList linkedlist 
    
    /* Create our local listening socket from first available address */
    for (addr = *addressList; addr != NULL; addr = addr->ai_next) {
        // Find available socket in address list
        if ((*socketfd = socket(addr->ai_family, addr->ai_socktype, addr->ai_protocol)) == -1) {
            perror("Failed to obtain socket descriptor");
            continue;
        }

        /* Set socket options to reuse address even if process crashes or has been killed */
        /* http://stackoverflow.com/a/24194999 */
        if (setsockopt(*socketfd, SOL_SOCKET, SO_REUSEADDR, &(int){1}, sizeof(int)) == -1) {
            perror("Error in setting socket options");
            return 0;
        }
        
        /* Bind or Connect to Socket */
        if (doBind == 1) {
            /* Attempt to bind to current socket, otherwise try another */
            if (bind(*socketfd, addr->ai_addr, addr->ai_addrlen) == -1) {
                close(*socketfd);
                perror("Failed to bind to socket");
                continue;
            }
        }
        else if (doBind == 0) {
            /* Attempt to connect to current socket, otherwise try another */
            if (connect(*socketfd, addr->ai_addr, addr->ai_addrlen) == -1) {
                perror("Failed to connect to socket");
            }
        }
        break;
    }

    /* free addressList since we don't need it */
    freeaddrinfo(*addressList);

    /* Check bind or success */
    if (addr == NULL) {
        fprintf(stderr, "Server failed to bind or connect\n");
        return 0;
    }
    return 1;
}


void waitForConnection(int serverSocket, char **port) {
    struct sockaddr_storage their_addr;     // helper struct to hold sockaddr struct
    socklen_t sin_size;                     // size of our incoming socket
    int clientSocket;                       // incoming socket file descriptor
    char their_ip[INET6_ADDRSTRLEN];        // array of incoming network addresses we have opened connections with
    int childProcessID;                     // PID of child process
    
    while (1) {
        sin_size = sizeof their_addr;
        /* accept new connection on our socket, cast their address as sockaddr to place in sockaddr_storage */
        clientSocket = accept(serverSocket, (struct sockaddr *)&their_addr, &sin_size);
        
        /* keep looking for new connections if this one failed */
        if (clientSocket == -1) {
            perror("accept");
            continue;
        }

        /* We have a valid accepted session now handle it */
        if ((childProcessID = fork()) == 0) {
            close(serverSocket); // child process doesn't need this any longer
            inet_ntop(their_addr.ss_family, getAddress((struct sockaddr *)&their_addr), their_ip, sizeof their_ip);
            struct socket *controlSocket = malloc(sizeof(struct socket));
            controlSocket->address = their_ip;
            controlSocket->socketfd = clientSocket;
            controlSocket->port = *port;
            waitForCommands(controlSocket);
            close(clientSocket);
            free(controlSocket);
            exit(EXIT_SUCCESS); // kill child process
        }
        close(clientSocket);
    }
}

void *getAddress(struct sockaddr *sa) {
    if (sa->sa_family == AF_INET) {
        return &(((struct sockaddr_in*)sa)->sin_addr);
    }

    return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

void waitForCommands(struct socket *controlSocket) {
    struct socket *dataSocket = malloc(sizeof(struct socket));
    dataSocket->address = controlSocket->address;
    int data_size = INT_SEND_SIZE;
    int received;
    
    /* receive encryption type */
    char encryption_type[data_size+1];
    memset(&encryption_type, 0, sizeof(encryption_type));
    while ((received = recv(controlSocket->socketfd, encryption_type, data_size, 0))) {
        if (received == data_size) break;
    }
    encryption_type[received] = '\0';

    if (strcmp(encryption_type, "encrypt") != 0) {
        fprintf(stderr, "ERROR: otp_dec cannot use otp_enc_d!\n");
        exit(1);
    }

    /* receive textfile size */
    char str_text_file_size[data_size+1];
    memset(&str_text_file_size, 0, sizeof(str_text_file_size));
    while ((received = recv(controlSocket->socketfd, str_text_file_size, data_size, 0))) {
        if (received == data_size) break;
    }
    str_text_file_size[received] = '\0';

    /* receive textfile and store to string */
    int text_str_size = str_to_int(str_text_file_size);
    char text_str[text_str_size+1];
    memset(&text_str, 0, sizeof(text_str));
    while ((received = recv(controlSocket->socketfd, text_str, text_str_size, 0))) {
        if (received == text_str_size) break;
    }
    text_str[received] = '\0';

    /* receive keyfile size */
    char str_key_file_size[data_size+1];
    memset(&str_key_file_size, 0, sizeof(str_key_file_size));
    while ((received = recv(controlSocket->socketfd, str_key_file_size, data_size, 0))) {
        if (received == data_size) break;
    }
    str_key_file_size[received] = '\0';

    /* receive keyfile and store to string */
    int key_str_size = str_to_int(str_key_file_size);
    char key_str[key_str_size+1];
    memset(&key_str, 0, sizeof(key_str));
    while ((received = recv(controlSocket->socketfd, key_str, key_str_size, 0))) {
        if (received == key_str_size) break;
    }
    key_str[received] = '\0';
    
    /* encrypt textfile and send back */
    char *encrypted = encrypt_with_str(text_str, key_str);
    
    /* send the file size of the cipher */ 
    char cipher_size[sizeof(int)];
    sprintf(cipher_size, "%zu", strlen(encrypted));
    int bytes_sent = write(controlSocket->socketfd, &cipher_size, data_size);
    if (bytes_sent < 0) perror("error sending file size of cipher");

    /* send the cipher file */
    while ((bytes_sent = write(controlSocket->socketfd, encrypted, strlen(encrypted))) > 0);
}

void setupDataSocket(struct socket **dataSocket) {
    struct addrinfo *addressList;
    int setSucc = setAddressList((*dataSocket)->address, (*dataSocket)->port, &addressList);
    int connSucc = bindOrConnect(&((*dataSocket)->socketfd), &addressList, 0);
    if (setSucc != 1 && connSucc != 1) {
        fprintf(stderr, "Failed to open data socket, exiting...");
        exit(EXIT_FAILURE);
    }
}

void sigchild_handler(int sig) {
    // waitpid() might overwrite errno, so we save and restore it:
    sig = sig; // nothing to do with this and dumping so compiler doesn't complain
    int saved_errno = errno;
    while(waitpid(-1, NULL, WNOHANG) > 0);
    errno = saved_errno;
}
