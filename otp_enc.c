/*************************************************************************************************************
 * Filename: otp_enc.c
 * Author: Jens Bodal
 * Date: June 5, 2016
 * Description: one-time pad encryption program; sends file and keyfile to be encrypted and outputs the 
 * encrypted contents
 ************************************************************************************************************/

#include <errno.h>          // stderr
#include <fcntl.h>          // file macros
#include <limits.h>         // INT_MAX
#include <stdio.h>          // fprintf
#include <stdlib.h>         // exit
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/sendfile.h>   // sendfile
#include <sys/socket.h>
#include <sys/stat.h>      // fstat
#include <netinet/in.h>
#include <netdb.h> 

#include "otp_engine.h"

int CMD_ERROR = 1;
int READ_ERROR = 1;
int VALID_ERROR = 1;

int openconnection(char *host, int portno) {
    struct sockaddr_in serv_addr;
    struct hostent *server;

    int sockfd = socket(AF_INET, SOCK_STREAM, 0);

    if (sockfd < 0) {
        perror("ERROR opening socket");
    }
    
    server = gethostbyname(host);
    
    if (server == NULL) {
        fprintf(stderr,"ERROR, no such host\n");
        exit(0);
    }
    
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    
    bcopy((char *)server->h_addr, (char *)&serv_addr.sin_addr.s_addr, server->h_length);
    serv_addr.sin_port = htons(portno);

    if (connect(sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0) {
        perror("ERROR connecting");
    }

    return sockfd;
}

int main(int argc, char *argv[]) {
    int sockfd, portno;
    struct stat file_buffer;
    struct stat key_buffer;
    int file_fd;
    int key_fd;
    char *filepath = strdup(argv[1]);
    char *keypath = strdup(argv[2]);

    // perform file and input error checking
    if (argc != 4) {
        fprintf(stderr, "Usage: %s (filepath) (keypath) (otp_enc_d port)\n", argv[0]);
        exit(CMD_ERROR);
    }

    if ((file_fd = open(filepath, O_RDONLY)) < 0) {
        fprintf(stderr, "ERROR opening [%s]\n", filepath);
        exit(READ_ERROR);
    }
    
    if ((key_fd = open(keypath, O_RDONLY)) < 0) {
        fprintf(stderr, "ERROR opening [%s]\n", keypath);
        exit(READ_ERROR);
    }
    
    if (validate_key_fpath(filepath, keypath) == -1) {
        fprintf(stderr, "ERROR: filepath or keypath have either incorrect characters or lengths\n");
        exit(VALID_ERROR);
    }

    portno = atoi(argv[3]);
    sockfd = openconnection("localhost", portno);

    fstat(file_fd, &file_buffer);
    fstat(key_fd, &key_buffer);
    int bytes_sent;

    // send encryption type
    char *encryption_type = "encrypt";
    bytes_sent = write(sockfd, encryption_type, INT_SEND_SIZE);
    if (bytes_sent < 0) perror("error sending the encryption type");

    // send textfile size
    char file_size[sizeof((int)file_buffer.st_size)];
    sprintf(file_size, "%d", (int) file_buffer.st_size);
    bytes_sent = write(sockfd, &file_size, INT_SEND_SIZE);
    if (bytes_sent < 0) perror("error sending textfile size");

    // send textfile
    bytes_sent = sendfile(sockfd, file_fd, 0, file_buffer.st_size);
    if (bytes_sent < 0) perror("error sending textfile file");
    
    // send keyfile size
    char key_size[sizeof((int)key_buffer.st_size)];
    sprintf(key_size, "%d", (int) key_buffer.st_size);
    bytes_sent = write(sockfd, &key_size, INT_SEND_SIZE);
    if (bytes_sent < 0) perror("error sending keyfile size");

    // send keyfile
    bytes_sent = sendfile(sockfd, key_fd, 0, key_buffer.st_size);

    /* wait for encrypted file to be sent back */
    /* get incoming file size */
    char incoming_size[INT_SEND_SIZE+1];
    int bytes_rec;
    while ((bytes_rec = recv(sockfd, incoming_size, INT_SEND_SIZE, 0))) {
        if (bytes_rec == INT_SEND_SIZE) break;   
    }
    incoming_size[bytes_rec] = '\0';

    /* receive cipher text */
    int cipher_size = str_to_int(incoming_size);
    char cipherstr[cipher_size+1];
    memset(&cipherstr, 0, sizeof(cipherstr));
    while ((bytes_rec = recv(sockfd, cipherstr, cipher_size, 0))) {
        if (bytes_rec == cipher_size) break;
    }
    cipherstr[bytes_rec] = '\0';
    
    // return the cipher string
    fprintf(stdout, "%s", cipherstr);

    // close up shop
    close(sockfd);
    close(file_fd);
    close(key_fd);
    return 0;
}
