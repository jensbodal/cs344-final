/*************************************************************************************************************
 * Filename: keygen.c
 * Author: Jens Bodal
 * Date: June 5, 2016
 * Description: creates a key file of specified length for use in a one-time pad system
 ************************************************************************************************************/

#include <stdlib.h>     // srand, rand
#include <stdio.h>      // printf @todo delete
#include <time.h>       // time


/*************************************************************************************************************
 * Description: gets a random integer in the specified range
 * @param min the minimum number to return
 * @param max the maximum number to return
 * @return random integer in the specified range
 ************************************************************************************************************/
int getRandom(int min, int max);

int main(int argc, char *argv[]) {
    if (argc != 2) {
        fputs("Incorrect number of arguments given\n", stderr);
        exit(1);
    }
    int keyLength = atoi(argv[1]);
    // +1 for including Z, +1 for including ' ' (space)
    int numLetters = 'Z' - 'A' + 2;
    char letters[numLetters];
    int i;
    
    srand(time(NULL));
     
    // fill array with our letters 
    for (i = 'A'; i <= 'Z'; i++) {
        letters[i-'A'] = (char)i;
    }
    // add space
    letters[numLetters-1] = ' ';
    
    for (i = 0; i < keyLength; i++) {
        printf("%c", letters[getRandom(0, numLetters-1)]);
    }
    fputs("\n", stdout);
    return 0;
}

int getRandom(int min, int max) {
    return (rand() % (max - min + 1) + (min));
}
