/*************************************************************************************************************
 * Filename: otp_engine.h
 * Author: Jens Bodal
 * Date: May 29, 2016
 * Description: 
 ************************************************************************************************************/

#ifndef OTP_ENGINE_H
#define OTP_ENGINE_H

extern const char *VALID_LETTERS;
extern const int INT_SEND_SIZE;

/*************************************************************************************************************
 * Description: takes the file path to a file and returns its contents as a string
 * @param fpath the string to the file path
 * @return cstring of file contents
 * source: http://stackoverflow.com/a/7856790/679716 
 ************************************************************************************************************/
char *fileToString(char *fpath);

/*************************************************************************************************************
 * Description: encrypts a string with the given keyfile
 * @param input_text the text to encrypt 
 * @param key_text the key to use to encrypt the text
 * @return encrypted string of text
 ************************************************************************************************************/
char *encrypt_with_str(char *input_text, char *key_text); 

/*************************************************************************************************************
 * Description: encrypts the contents of a file with the given key file 
 * @param input_fpath the file path to the file to encrypt 
 * @param key_fpath the file path to the key to use to encrypt the file
 * @return a string representation of the encrypted contents 
 ************************************************************************************************************/
char *encrypt_with_fpath(char *input_fpath, char *key_fpath);

/*************************************************************************************************************
 * Description: decrypts a string with the given keyfile
 * @param input_text the text to decrypt 
 * @param key_text the key to use to decrypt the text
 * @return decrypted string of text
 ************************************************************************************************************/
char *decrypt_with_str(char *input_text, char *key_text);

/*************************************************************************************************************
* Description: decrypts the contents of a file with the given key file 
 * @param input_fpath the file path to the file to decrypt 
 * @param key_fpath the file path to the key to use to decrypt the file
 * @return a string representation of the decrypted contents 
 ************************************************************************************************************/
char *decrypt_with_fpath(char *input_fpath, char *key_fpath);

/*************************************************************************************************************
 * Description: ensures the given key and input text are valid
 * @param input_text the text to validate
 * @param key_text the key contents to validate
 * @return 1 if valid -1 if not valid
 ************************************************************************************************************/
int validate_key_str(char *input_text, char *key_text);

/*************************************************************************************************************
 * Description: validates the given key path and input file path
 * @param input_fpath the file path of the file to validate 
 * @param key_fpath the file path of the key to validate
 * @return 1 if valid -1 if not valid
 ************************************************************************************************************/
int validate_key_fpath(char *input_fpath, char *key_fpath);


/*************************************************************************************************************
 * Description: 
 * @param 
 * @return 
 ************************************************************************************************************/
int str_to_int(char *str);

/** INTERNAL FUNCTIONS **/
char _modulate(int char_int);
int _get_letter_index(char c);
int _doIgnoreChar(char c);


#endif
