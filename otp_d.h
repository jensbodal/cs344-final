/*************************************************************************************************************
 * Filename: otp_d.h
 * Author: Jens Bodal
 * Date: June 5, 2016
 * Description: program header file for one time pad daemons
 * Provides socket struct implementation
 ************************************************************************************************************/

#ifndef OTP_D_H
#define OTP_D_H

/***************************************************************************************************
 * Description: the socket struct is used to store information about various sockets so we can 
 * easily pass them around and access their related variables
 **************************************************************************************************/
struct socket {
    char *address;
    char *port;
    int socketfd;
};

/***************************************************************************************************
 * Description: Validates the provided port parameter that it is within
 * a specified range provided by the implementation
 * @param port cstring representation of port
 * @return 1 if port is valid and within range specified by implementation
 **************************************************************************************************/
int isValidPort(char *port);

/***************************************************************************************************
 * Description: Attempts to start our server on the provided port and to
 * open a socket with the given port and start listening
 * Majority of code taken from Beej's (see readme)
 * @param port cstring of port to open our socket on 
 **************************************************************************************************/
void startServer(char *port);

/***************************************************************************************************
 * Description: Sets a pointer to an addrinfo struct to become a 
 * linkedlist of possible addrinfo structs to bind to
 * addrinfo setup: http://man7.org/linux/man-pages/man3/getaddrinfo.3.html
 * Error check: http://linux.die.net/man/3/gai_strerror
 * @param host cstring of hostname to associate socket with, if NULL bind to localhost
 * @param port cstring of port to associate socket with
 * @param addressList the struct to return our linkedlist to
 * @return 1 if have address(es) to bind to, 0 if not
 **************************************************************************************************/
 int setAddressList(char *host, char *port, struct addrinfo **addressList);

/***************************************************************************************************
 * Description: Attempts to bind or connect to a socket and sets the socket file descriptor 
 * accordingly
 * Modified from Beej's guide
 * @param socketfd socket file descriptor to bind or connect to
 * @param addressList pointer to the struct to return our linkedlist to
 * @param doBind set to 1 if we are binding, 0 if we are connecting
 * @return 1 if bound to socket, 0 if connected to socket
 **************************************************************************************************/
int bindOrConnect(int *socketfd, struct addrinfo **addressList, int doBind);
 
/***************************************************************************************************
 * Description: Assumes we have an open socket and are listening on it and attempts to bind any new 
 * connections to a new process
 * Majority of code portions taken from Beej's (see references in readme)
 * @param serverSocket the socket file descriptor we have opened and are listening on
 * @param port the port the socket file descriptor is associated with
 **************************************************************************************************/
void waitForConnection(int serverSocket, char **port);

/***************************************************************************************************
 * Description: Function taken from Beej's (see readme)
 * Taken from Beej's guide
 * @param sa pointer to sockaddr struct which we want to obtain the address from
 **************************************************************************************************/
void *getAddress(struct sockaddr *sa);

/***************************************************************************************************
 * Description: Waits for a message from the client socket then processes them
 * @param controlSocket the client socket that has connected to us
 **************************************************************************************************/
void waitForCommands(struct socket *controlSocket);

/***************************************************************************************************
 * Description: Sets up our data socket by filling in necessary information then connecting to it
 * @param dataSocket the data socket that we are going to create and connect to
 **************************************************************************************************/
void setupDataSocket(struct socket **dataSocket);

/***************************************************************************************************
 * Description: Cleans up zombie processes but does not do anything with their information.
 * Taken entirely from Beej's (see references in readme).  
 * @param none
 **************************************************************************************************/
void sigchild_handler(int sig);

#endif
