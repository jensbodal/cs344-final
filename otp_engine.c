/*************************************************************************************************************
 * Filename: otp_engine.c
 * Author: Jens Bodal
 * Date: June 05, 2016
 * Description: one-time pad engine for encryption and decryption 
 ************************************************************************************************************/

#include <stdio.h>      // FILE, fseek, fopen, rewind
#include <stdlib.h>     // malloc
#include <string.h>     // strlen

#include "otp_engine.h"

const char *VALID_LETTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ ";
const int INT_SEND_SIZE = 256;

//int main() {
//    char *input_text = "plaintext3";
//    char *key = "mykey";
//    char *encrypted_text = encrypt_with_fpath(input_text, key);
//    char *decrypted_text = decrypt_with_str(encrypted_text, fileToString(key));
//
//    printf("ORIGINAL: %s\n", fileToString(input_text));
//    printf("Encrypted: %s\n", encrypted_text);
//    printf("DECRYPTED: %s\n", decrypted_text);
//    return 0;
//}

char *encrypt_with_str(char *input_text, char *key_text) {
    char *temp_text = input_text;
    char *temp_key = key_text;
    char *encrypted_output = malloc(strlen(input_text) * (sizeof(char*)));
    strcpy(encrypted_output, input_text);
    int i = 0;

    while (*temp_text) {
        char c;
        if (*temp_text  == '\n') {
            c = *temp_text;
        }
        else {
            int text_char = _get_letter_index(*temp_text);
            int key_char = _get_letter_index(*temp_key);
            c = _modulate(text_char+key_char);
        }
        
        encrypted_output[i] = c;
        temp_text++;
        temp_key++;
        i++;
    }
    encrypted_output[i] = '\0';    
    return encrypted_output;
}

char *encrypt_with_fpath(char *input_fpath, char *key_fpath) {
   return encrypt_with_str(fileToString(input_fpath), fileToString(key_fpath));
}

char *decrypt_with_str(char *input_text, char *key_text) {
    char *temp_text = input_text;
    char *temp_key = key_text;
    char *decrypted_output = strdup(input_text);
    int i = 0;
    while (*temp_text) {
        char c;
        if (*temp_text  == '\n') {
            c = *temp_text;
        }
        else {
            int text_char = _get_letter_index(*temp_text);
            int key_char = _get_letter_index(*temp_key);
            c = _modulate(text_char-key_char);
        }
        
        decrypted_output[i] = c;
        temp_text++;
        temp_key++;
        i++;
    }
    decrypted_output[i] = '\0';    
    return decrypted_output;
}

char *decrypt_with_fpath(char *input_fpath, char *key_fpath) {
   return decrypt_with_str(fileToString(input_fpath), fileToString(key_fpath));
}

char *fileToString(char *fpath) {
    char *file_contents;
    size_t input_file_size;
    FILE *input_file = fopen(fpath, "rb");
    
    // get length of file
    fseek(input_file, 0, SEEK_END);
    input_file_size = ftell(input_file);
    rewind(input_file);

    // allocate enough memory for our string
    file_contents = malloc(input_file_size * (sizeof(char)));

    // read file into string
    fread(file_contents, sizeof(char), input_file_size, input_file);
    fclose(input_file);

    // return string representation of file
    return file_contents;
}

int validate_key_str(char *input_text, char *key_text) {
    char *temp = input_text;
    while (*temp) {
        if (!_doIgnoreChar(*temp) && _get_letter_index(*temp) == -1) {
            return -1;
        }
        temp++;
    }
    if (strlen(input_text) > strlen(key_text)) return -1;

    return 1;
}

int validate_key_fpath(char *input_fpath, char *key_fpath) {
    return validate_key_str(fileToString(input_fpath), fileToString(key_fpath));
}

int str_to_int(char *str) {
    int tens = 1;
    int num = 0;
    char *temp = str;

    while (*temp) {
        temp++;
    }
    temp--;

    while (*temp) {
        if (*temp >= '0' && *temp <= '9') {
            num += (tens * (*temp - '0'));
            tens*=10;
        }
        temp--;
    }
    return num;
}

char _modulate(int char_int) {
    int modulus = strlen(VALID_LETTERS);
    int new_char;

    if (char_int < 0) {
        new_char = char_int + modulus;    
    }
    else {
        new_char = char_int % modulus;
    }
    return VALID_LETTERS[new_char];
}

int _get_letter_index(char c) {
    const char *temp = VALID_LETTERS;
    int i = 0;

    while (*temp) {
        if (c == *temp) {
            return i;
        }
        temp++;
        i++;
    }

    return -1;
}

int _doIgnoreChar(char c) {
    return (c == '\n' || c == '\r');
}
